package pizzashop.service;

import org.junit.jupiter.api.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PizzaServiceIntegrationWithPaymentRepository {

    @Spy
    private PizzaService pizzaService;
    private static List<Payment> initialPayments;
    private static final ClassLoader repositoryClassLoader = PaymentRepository.class.getClassLoader();
    public static final String filename = "data/payments.txt";

    @BeforeAll
    static void firstSetUp() {

        PaymentRepository paymentRepository = new PaymentRepository();
        initialPayments = new ArrayList<>(paymentRepository.getAll());
    }

    @AfterAll
    static void theLastTearDown() {

        PaymentRepository paymentRepository = new PaymentRepository();
        initialPayments.forEach(payment -> {
            try {
                paymentRepository.add(payment);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @BeforeEach
    void setUp() {

        MenuRepository menuRepository = new MenuRepository();
        PaymentRepository paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);

        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown() {

        File file = null;
        try {
            file = new File(repositoryClassLoader.getResource(filename).toURI().getPath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Assertions.assertNotNull(file);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            //delete all from file
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldSuccessWhenAddPaymentOnTableNumber1() {

        Payment payment = mock(Payment.class);
        when(payment.getType()).thenReturn(PaymentType.CARD);
        when(payment.getTableNumber()).thenReturn(1);
        when(payment.getAmount()).thenReturn(12.2d);

        when(pizzaService.createPayment(1, PaymentType.CARD, 12.2d)).thenReturn(payment);

        int numberOfPayments = pizzaService.getPayments().size();
        Assertions.assertDoesNotThrow(() -> {
            pizzaService.addPayment(1, PaymentType.CARD, 12.2d);

            List<Payment> payments = pizzaService.getPayments();
            Assertions.assertEquals(numberOfPayments + 1, payments.size());

            Payment savedPayment = payments.get(numberOfPayments);
            Assertions.assertEquals(1, savedPayment.getTableNumber());
            Assertions.assertEquals(12.2d, savedPayment.getAmount());
            Assertions.assertEquals(PaymentType.CARD, savedPayment.getType());
        });
    }

    @Test
    public void shouldSuccessWhenAddPaymentOnTableNumber8(){

        Payment payment = mock(Payment.class);
        when(payment.getType()).thenReturn(PaymentType.CASH);
        when(payment.getTableNumber()).thenReturn(8);
        when(payment.getAmount()).thenReturn(15d);

        when(pizzaService.createPayment(anyInt(), any(), anyDouble())).thenReturn(payment);

        int numberOfPayments = pizzaService.getPayments().size();
        Assertions.assertDoesNotThrow(() -> {
            pizzaService.addPayment(1, PaymentType.CARD, 12.2d);

            List<Payment> payments = pizzaService.getPayments();
            Assertions.assertEquals(numberOfPayments + 1, payments.size());

            Payment savedPayment = payments.get(numberOfPayments);
            Assertions.assertEquals(8, savedPayment.getTableNumber());
            Assertions.assertEquals(15d, savedPayment.getAmount());
            Assertions.assertEquals(PaymentType.CASH, savedPayment.getType());
        });
    }
}
