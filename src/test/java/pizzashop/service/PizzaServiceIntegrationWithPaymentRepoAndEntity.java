package pizzashop.service;

import org.junit.jupiter.api.*;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PizzaServiceIntegrationWithPaymentRepoAndEntity {

    private PizzaService pizzaService;
    private static List<Payment> initialPayments;
    private static final ClassLoader repositoryClassLoader = PaymentRepository.class.getClassLoader();
    public static final String filename = "data/payments.txt";

    @BeforeAll
    static void firstSetUp() {

        PaymentRepository paymentRepository = new PaymentRepository();
        initialPayments = new ArrayList<>(paymentRepository.getAll());
    }

    @AfterAll
    static void theLastTearDown() {

        PaymentRepository paymentRepository = new PaymentRepository();
        initialPayments.forEach(payment -> {
            try {
                paymentRepository.add(payment);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @BeforeEach
    @AfterEach
    void setupAndTearDown() {

        File file = null;
        try {
            file = new File(repositoryClassLoader.getResource(filename).toURI().getPath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Assertions.assertNotNull(file);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            //delete all from file
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEmptyList() {

        List<Payment> payments = new ArrayList<>();
        setup(payments);
        Assertions.assertEquals(0, pizzaService.getTotalAmount(PaymentType.CASH), "Total amount must be " + 0 + "!");
    }

    @Test
    public void testListWithOnePayment() {

        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(1, PaymentType.CASH, 10));
        setup(payments);
        Assertions.assertEquals(10, pizzaService.getTotalAmount(PaymentType.CASH), "Total amount must be " + 10 + "!");
    }

    @Test
    public void testListWithTwoPaymentsCashAndCard() {

        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(1, PaymentType.CASH, 10));
        payments.add(new Payment(1, PaymentType.CARD, 20));
        setup(payments);
        Assertions.assertEquals(10, pizzaService.getTotalAmount(PaymentType.CASH), "Total amount must be " + 10 + "!");
    }

    @Test
    public void testListWithThreePaymentsCashAndCardAndCash() {

        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(1, PaymentType.CASH, 10));
        payments.add(new Payment(1, PaymentType.CARD, 20));
        payments.add(new Payment(1, PaymentType.CASH, 15.5));
        setup(payments);
        Assertions.assertEquals(25.5, pizzaService.getTotalAmount(PaymentType.CASH), "Total amount must be " + 25.5 + "!");
    }

    @Test
    public void shouldSuccessWhenAddPayment(){

        List<Payment> payments = Collections.singletonList(new Payment(1,PaymentType.CARD, 1d));
        setup(payments);
        Assertions.assertDoesNotThrow(() -> pizzaService.addPayment(2, PaymentType.CASH, 2d));
        payments = pizzaService.getPayments();
        Assertions.assertEquals(2, payments.size());
        Assertions.assertEquals(2,payments.get(1).getTableNumber());
        Assertions.assertEquals(2d,payments.get(1).getAmount());
        Assertions.assertEquals(PaymentType.CASH,payments.get(1).getType());
    }

    private void setup(List<Payment> payments) {

        MenuRepository menuRepository = new MenuRepository();
        PaymentRepository paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
        payments.forEach(payment -> pizzaService.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
    }
}
