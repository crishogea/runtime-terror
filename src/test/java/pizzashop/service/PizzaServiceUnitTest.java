package pizzashop.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.io.IOException;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

public class PizzaServiceUnitTest {

    @Spy
    private PaymentRepository paymentRepository;

    @InjectMocks
    private PizzaService pizzaService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldSuccessWhenAddAPayment() throws IOException {
        Payment payment = new Payment(1, PaymentType.CARD, 23.5d);
        doNothing().when(paymentRepository).writeAll();
        int actualPaymentsNo = pizzaService.getPayments().size();
        Assertions.assertDoesNotThrow(() -> {
            pizzaService.addPayment(payment.getTableNumber(),payment.getType(), payment.getAmount());
            List<Payment> foundPayments = pizzaService.getPayments();
            Assertions.assertEquals(actualPaymentsNo + 1, foundPayments.size());

            Payment savedPayment = foundPayments.get(actualPaymentsNo);
            Assertions.assertEquals(payment.getTableNumber(),savedPayment.getTableNumber());
            Assertions.assertEquals(payment.getType(),savedPayment.getType());
            Assertions.assertEquals(payment.getAmount(),savedPayment.getAmount());
        });
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenTableIsLessThanOne() {

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            pizzaService.addPayment(0,PaymentType.CARD, 15.333d);
        });
        Assertions.assertEquals("Table must be greater than 0", exception.getMessage());
    }

    @Test
    void shouldThrowRunTimeExceptionWhenCanNotWriteToFile() throws IOException {

        Payment payment = new Payment(1, PaymentType.CARD, 23.5d);
        int actualPaymentsNo = pizzaService.getPayments().size();
        doThrow(new IOException()).when(paymentRepository).writeAll();
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class, () -> pizzaService.addPayment(payment.getTableNumber(),payment.getType(), payment.getAmount()));
        Assertions.assertEquals("Payment could not be stored on disk!", exception.getMessage());

        List<Payment> foundPayments = pizzaService.getPayments();
        Assertions.assertEquals(actualPaymentsNo + 1, foundPayments.size());

        Payment savedPayment = foundPayments.get(actualPaymentsNo);
        Assertions.assertEquals(payment.getTableNumber(),savedPayment.getTableNumber());
        Assertions.assertEquals(payment.getType(),savedPayment.getType());
        Assertions.assertEquals(payment.getAmount(),savedPayment.getAmount());
    }
}
