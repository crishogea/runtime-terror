package pizzashop.service;

import org.junit.jupiter.api.*;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class PizzaServiceTest {

    private PizzaService pizzaService;
    private List<Payment> initialPayments;
    private static final ClassLoader repositoryClassLoader = PaymentRepository.class.getClassLoader();
    public static final String filename = "data/payments.txt";

    @BeforeEach
    void setUp() {

        MenuRepository menuRepository = new MenuRepository();
        PaymentRepository paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
        initialPayments = new ArrayList<>(pizzaService.getPayments());
    }

    @AfterEach
    void tearDown() {

        File file = null;
        try {
            file = new File(repositoryClassLoader.getResource(filename).toURI().getPath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Assertions.assertNotNull(file);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (Payment p : initialPayments) {
                System.out.println(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldSuccessWhenAddingPayment() {

        int table= 1;
        Assertions.assertDoesNotThrow(() -> {
            pizzaService.addPayment(table, PaymentType.CASH, 0.5d);
            List<Payment> payments = pizzaService.getPayments();
            Payment lastPayment = payments.get(payments.size() - 1);
            Assertions.assertEquals(table, lastPayment.getTableNumber(), "Table number must be " + table);
            Assertions.assertEquals(PaymentType.CASH, lastPayment.getType(), "Payment type must be " + PaymentType.CASH);
            Assertions.assertEquals(0.5d, lastPayment.getAmount(), "Amount must be " + lastPayment.getAmount());
        });
    }

    @Test
    @Tag(value = "Throw-IllegalArgumentException")
    public void shouldThrowIllegalArgumentExceptionWhenTableIs0() {

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            pizzaService.addPayment(0, PaymentType.CASH, 45.67d);
        });
        Assertions.assertEquals("Table must be greater than 0", exception.getMessage());
    }

    @Test
    @Tag(value = "Throw-IllegalArgumentException")
    public void shouldThrowIllegalArgumentExceptionWhenTableIs9() {

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            pizzaService.addPayment(9, PaymentType.CASH, 45.67d);
        });
        Assertions.assertEquals("Table must be less than 9", exception.getMessage());
    }

    @Test
    @Tag(value = "Throw-IllegalArgumentException")
    public void shouldThrowIllegalArgumentExceptionWhenAmountIs0() {

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            pizzaService.addPayment(3, PaymentType.CASH, 0d);
        });
        Assertions.assertEquals("Amount must be greater than 0", exception.getMessage());
    }

    @Test
    public void shouldSuccessWhenAmountIsLessThanMax() {

        Assertions.assertDoesNotThrow(() -> {
            pizzaService.addPayment(2, PaymentType.CASH, Double.MAX_VALUE - 1);
            List<Payment> payments = pizzaService.getPayments();
            Payment lastPayment = payments.get(payments.size() - 1);
            Assertions.assertEquals(2, lastPayment.getTableNumber(), "Table number must be 2");
            Assertions.assertEquals(PaymentType.CASH, lastPayment.getType(), "Payment type must be " + PaymentType.CASH);
            Assertions.assertEquals(Double.MAX_VALUE - 1, lastPayment.getAmount(), "Amount must be " + (Double.MAX_VALUE - 1));
        });
    }

    @Test
    public void shouldSuccessWhenAmountIsMax() {

        Assertions.assertDoesNotThrow(() -> {
            pizzaService.addPayment(2, PaymentType.CASH, Double.MAX_VALUE);
            List<Payment> payments = pizzaService.getPayments();
            Payment lastPayment = payments.get(payments.size() - 1);
            Assertions.assertEquals(2, lastPayment.getTableNumber(), "Table number must be 2");
            Assertions.assertEquals(PaymentType.CASH, lastPayment.getType(), "Payment type must be " + PaymentType.CASH);
            Assertions.assertEquals(Double.MAX_VALUE, lastPayment.getAmount(), "Amount must be " + Double.MAX_VALUE);
        });
    }
}
