package pizzashop.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.io.IOException;
import java.util.List;

import static org.mockito.Mockito.*;

public class PaymentRepositoryUnitTest {

    @Spy
    private PaymentRepository paymentRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldSuccessWhenAddPayment() throws IOException {

        doNothing().when(paymentRepository).writeAll();

        Payment payment = mock(Payment.class);
        when(payment.getAmount()).thenReturn(10d);
        when(payment.getTableNumber()).thenReturn(1);
        when(payment.getType()).thenReturn(PaymentType.CARD);

        int actualPaymentsNo = paymentRepository.getAll().size();
        Assertions.assertDoesNotThrow(() -> {
            paymentRepository.add(payment);
        });

        List<Payment> foundPayments = paymentRepository.getAll();
        Assertions.assertEquals(actualPaymentsNo + 1, foundPayments.size());

        Payment savedPayment = foundPayments.get(actualPaymentsNo);
        Assertions.assertEquals(payment.getTableNumber(),savedPayment.getTableNumber());
        Assertions.assertEquals(payment.getType(),savedPayment.getType());
        Assertions.assertEquals(payment.getAmount(),savedPayment.getAmount());
    }

    @Test
    void shouldThrowIOExceptionWhenCanNotWriteToFile() throws IOException {

        doThrow(IOException.class).when(paymentRepository).writeAll();

        Payment payment = mock(Payment.class);
        when(payment.getAmount()).thenReturn(10d);
        when(payment.getTableNumber()).thenReturn(1);
        when(payment.getType()).thenReturn(PaymentType.CARD);

        int actualPaymentsNo = paymentRepository.getAll().size();
        Assertions.assertThrows(IOException.class ,() -> {
            paymentRepository.add(payment);
        });

        List<Payment> foundPayments = paymentRepository.getAll();
        Assertions.assertEquals(actualPaymentsNo + 1, foundPayments.size());

        Payment savedPayment = foundPayments.get(actualPaymentsNo);
        Assertions.assertEquals(payment.getTableNumber(),savedPayment.getTableNumber());
        Assertions.assertEquals(payment.getType(),savedPayment.getType());
        Assertions.assertEquals(payment.getAmount(),savedPayment.getAmount());
    }
}
