package pizzashop.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PaymentTest {

    private Payment payment = new Payment(1, PaymentType.CARD, 11.1d);
    private Payment paymentForTestSet = new Payment(1, PaymentType.CARD, 11.1d);

    @Test
    public void testGetTableNumber(){

        Assertions.assertEquals(1, payment.getTableNumber());
    }

    @Test
    public void testGetType(){

        Assertions.assertEquals(PaymentType.CARD, payment.getType());
    }

    @Test
    public void testGetAmount() {

        Assertions.assertEquals(11.1d, payment.getAmount());
    }

    @Test
    public void testSetTableNumber(){

        paymentForTestSet.setTableNumber(2);
        Assertions.assertEquals(2, paymentForTestSet.getTableNumber());
    }

    @Test
    public void testSetType(){

        paymentForTestSet.setType(PaymentType.CASH);
        Assertions.assertEquals(PaymentType.CASH, paymentForTestSet.getType());
    }

    @Test
    public void testSetAmount() {

        paymentForTestSet.setAmount(10d);
        Assertions.assertEquals(10d, paymentForTestSet.getAmount());
    }

    @Test
    public void testToString() {

        String paymentToString = "1,CARD,11.1";
        Assertions.assertEquals(paymentToString, payment.toString());
    }
}
