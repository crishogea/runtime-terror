package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.IOException;
import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo){
        this.menuRepo=menuRepo;
        this.payRepo=payRepo;
    }

    public List<MenuDataModel> getMenuData(){return menuRepo.getMenu();}

    public List<Payment> getPayments(){return payRepo.getAll(); }

    /**
     * Metoda inregistreaza o noua plata
     * @param table - numarul mesei pentru care se inregistreaza plata
     * @param type - tipul de plata (CASH sau CARD)
     * @param amount - suma totala de plata
     * pre:
     *         - table apartine multimii {1, 2, 3, 4, 5, 6, 7, 8}
     *         - amount trebuie sa fie un numar real pozitiv > 0
     */
    public void addPayment(int table, PaymentType type, double amount){

        try {
            checkIfDataAreValid(table, amount);
            Payment payment= createPayment(table, type, amount);
            payRepo.add(payment);
        } catch (IOException e) {
            throw new RuntimeException("Payment could not be stored on disk!");
        }
    }

    protected Payment createPayment(int table, PaymentType type, double amount) {

        return new Payment(table, type, amount);
    }

    private void checkIfDataAreValid(int table, double amount) {

        if (table < 1){
            throw new IllegalArgumentException("Table must be greater than 0");
        }
        if (table > 8){
            throw new IllegalArgumentException("Table must be less than 9");
        }
        if (amount <= 0){
            throw new IllegalArgumentException("Amount must be greater than 0");
        }
    }

    public double getTotalAmount(PaymentType type){
        double total=0.0f;
        List<Payment> paymentsList=getPayments();
        if (paymentsList==null)
            return total;
        if (paymentsList.isEmpty())
            return total;
        for (Payment p:paymentsList){
            if (p.getType().equals(type))
                total+=p.getAmount();
        }
        return total;
    }

}
